# Introduction

Synchronizes the [Teenage Engineering OP-1's](https://www.teenageengineering.com/products/op-1)
album, drum, synth and tape directories (individually or as a whole)
via a [Raspberry Pi](https://www.raspberrypi.org/products/raspberry-pi-3-model-b/) with an attached
[AdaFruit 20x4 RGB LCD screen](https://www.adafruit.com/product/1110).

When started the LCD will be red indicating the OP-1 has not been detected. When connected and placed
into disk mode the OP-1 will be detected and the screen will turn green. The OP-1 can then be synchronized
by selecting the synchronize menu and selected what is to be synchronized (everything, album, drum,
synth, tape).

While synchronizing the LCD screen will turn yellow to indicate synchronization is underway. If an
error occurs the LCD screen will turn red, once complete the screen will return to green and display
a message that synchronization was successfully completed.

Once synchronized the files will become visible in the web interface by browsing to the Raspberry Pi's IP address (or hostname if set up).

# Installation

`$ python setup.py install`

## Example Usage

`$ python op1pisync.py -v info`

## Visual Guide

When turned on you are greeted with a _red_ screen indicating the OP-1 is not connected. 

![Initial State](http://ufostalker.com/images/op1pisync/not_connected.jpg)

Enter the primary menu and you will be able to see the local IP address of the Raspberry Pi (for viewing the interal browser, ssh, scp, etc.), have the abilty to reboot or shut down the Raspberry Pi.

![Settings Menu](http://ufostalker.com/images/op1pisync/settings_menu.jpg)

To start synching your OP-1 enter [DISK mode](https://www.teenageengineering.com/guides/op-1/song-rendering-and-connectivity#13.5)

![Entering DISK Mode](http://ufostalker.com/images/op1pisync/op-1_connecting_w.jpg)

Once [DISK mode](https://www.teenageengineering.com/guides/op-1/song-rendering-and-connectivity#13.5) has connected with the Raspberry PI the screen will turn _green_ indicating it is ready to start synching.

![DISK Mode Active](http://ufostalker.com/images/op1pisync/op-1_connected_w.jpg)

Select what is to be synchronized (Everything, Album, Tape, Drums, Synths)

![DISK Mode Active](http://ufostalker.com/images/op1pisync/op-1_ready_to_sync.jpg)

The screen will turn _yellow_ (it doesn't show here, but trust me it's yellow) and the selected items will be synchronized to the `~/op-1/[yyyy-MM-dd_HHmmss]` directory (this can be configured or sent to a external drive). If an error occurs during synchronization the screen will turn _red_.

![OP-1 Synchronizing](http://ufostalker.com/images/op1pisync/op-1_synching.jpg)

Once synchronization is complete the screen will turn _green_ and a message will be displayed.

![OP-1 Synchronized](http://ufostalker.com/images/op1pisync/op-1_synchronized_w.jpg)

The files are then available in the web interface on the Raspberry Pi

![Web Interface](http://ufostalker.com/images/op1pisync/web-interface.png)

### Additional Details and Warnings

* Disconnecting or reconnecting the OP-1 to the Raspberry Pi will change the color of the screen from _red_ to _green_ or vise-versa indicating the state of the connection.
* When mounted the actual OP-1 file system will be available at `~/op-1/mounted/` (this can be configured). Any changes you make to this directory are reflected on the OP-1 - be careful.  
* This has been tested with firmwares 219 and 225 without any issues to date, but as always your mileage may vary.



