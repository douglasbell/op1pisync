import os
from setuptools import setup


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "op1pisync",
    version = "0.0.1",
    author = "Doug Bell",
    author_email = "douglas.bell@gmail.com",
    description = ("Synchronizes the Teenage Engineering OP-1's (https://www.teenageengineering.com/products/op-1) "
                "album, drum, synh and tape directories (individually or as a whole) "
                "via a Raspberry Pi (https://www.raspberrypi.org/products/raspberry-pi-3-model-b/) with an attached " 
                "AdaFruit 20x4 RGB LCD screen (https://www.adafruit.com/product/498).."),
    license = "BSD",
    keywords = "teenage engineering op-1 adafruit lcd",
    url = "https://bitbucket.org/douglasbell/op1pisync",
    # packages=[''],
    install_requires=[
        'watchdog',  'Adafruit_CharLCD', 'adafruit_lcd_plate_menu'
    ],
    long_description=read('../README.md'),
    classifiers=[
        "Development Status :: 5 - Production",
        "Topic :: Utilities",
        "License :: OSI Approved :: BSD License",
    ],
)