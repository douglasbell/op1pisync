"""
Handles AdaFruit LCD display and related operations

Todo:
    Break the functions out into their own class
"""

import os
import time
import socket
import subprocess
import errno
import shutil
import logging
import datetime
import Adafruit_CharLCD as LCD

from adafruit_lcd_plate_menu import MenuNode


class Display:

    # Create reference to LCD Plate
    lcd = LCD.Adafruit_CharLCDPlate()

    # Default and current background color - red until OP-1 connects
    current_display_color = [1.0, 0.0, 0.0]

    # Default 30 seconds
    background_timeout_in_seconds = 120

    #  Dictionary of background colors
    colors = {'Red': (1.0, 0.0, 0.0),
                'Yellow': (1.0, 1.0, 0.0),
                'Magenta': (1.0, 0.0, 1.0),
                'White': (1.0, 1.0, 1.0),
                'Green': (0.0, 1.0, 0.0),
                'Cyan': (0.0, 1.0, 1.0),
                'Blue': (0.0, 0.0, 1.0),
              }

    def __init__(self):
        self.lcd.clear()
        self.lcd.set_color(self.current_display_color[0],
                           self.current_display_color[1],
                           self.current_display_color[2])

    def set_message(self, message):
        self.lcd.clear()
        self.lcd.message(message)

    def set_color(self, red, blue, green):
        self.lcd.set_color(red, blue, green)

    def backlight_on(self):
        self.lcd.set_backlight(1)

    def backlight_on(self, red, blue, green):
        self.lcd.set_backlight(1)
        self.lcd.set_color(red, blue, green)

    def backlight_off(self):
        self.lcd.set_backlight(0)

    def clear_display(self):
        self.lcd.clear()

    def create_menu_nodes(self):

        menu_nodes = []

        settings_menu_node = MenuNode('OP-1 Pi Sync')

        settings_menu_node.add_node(MenuNode('Internal IP', None, self.display_internal_ip))
        settings_menu_node.add_node(MenuNode('Reboot', None, self.reboot))
        settings_menu_node.add_node(MenuNode('Shutdown', None, self.shutdown))

        menu_nodes.append(settings_menu_node)

        synchronized_menu_node = MenuNode('Synchronize')
        synchronized_menu_node.add_node(MenuNode('Everything', None, self.synchronize, ''))
        synchronized_menu_node.add_node(MenuNode('Album', None, self.synchronize, 'album'))
        synchronized_menu_node.add_node(MenuNode('Tape', None, self.synchronize, 'tape'))
        synchronized_menu_node.add_node(MenuNode('Drum', None, self.synchronize, 'drum'))
        synchronized_menu_node.add_node(MenuNode('Synth', None, self.synchronize, 'synth'))

        menu_nodes.append(synchronized_menu_node)

        return menu_nodes

    # Internal IP
    def display_internal_ip(self, menu_display, selected_node, *argvs):
        try:
            if len(selected_node.nodes) > 0:
                del selected_node.nodes[0]
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            s.connect(('8.8.8.8', 0))
            internal_ip = s.getsockname()[0]
            selected_node.add_node(MenuNode(internal_ip))
        except socket.error as e:
            logging.error('Could not retrieve internal IP', e)
            selected_node.add_node(MenuNode('Unknown'))
        pass

     # Reboot
    def reboot(self, menu_display, selected_node, *argvs):
        try:
            if len(selected_node.nodes) > 0:
                del selected_node.nodes[0]
            self.backlight_off()
            subprocess.call(['reboot', '-h'], shell=True)
        except OSError as e:
            logging.error('Could not reboot', e)
            selected_node.add_node(MenuNode('Could not Reboot   '))

    # Shutdown
    def shutdown(self, menu_display, selected_node, *argvs):
        if len(selected_node.nodes) > 0:
            del selected_node.nodes[0]
        try:
            selected_node.add_node(MenuNode('Shutting Down... Please Wait 30 Secs   '))
            time.sleep(5)
            self.backlight_off()
            subprocess.call(['shutdown', '-h', 'now'], shell=True)
        except OSError as e:
            logging.error('Could not reboot', e)
            selected_node.add_node(MenuNode('Error Shutting Down   '))

    # Synchronize
    def synchronize(self, menu_display, selected_node, *argvs):

        if len(selected_node.nodes) > 0:
            del selected_node.nodes[0]

        directory = argvs[0];

        self.set_color(1.0, 1.0, 0.0)  # Set Display Yellow

        src = '/op-1/mounted/{0}/'.format(directory)
        current_date = datetime.datetime.today().strftime('%Y-%m-%d_%H%M%S')

        dst = '/op-1/{0}/{1}'.format(current_date, directory)

        logging.info('Synchronizing {0} to {1}...'.format(src, dst))

        try:
            if os.path.exists('/op-1/mounted/album'): # Ensure OP-1 is mounted
                shutil.copytree(src, dst)
                self.touch(dst)
                self.set_color(0.0, 1.0, 0.0)  # Set Display Green
                selected_node.add_node(MenuNode('Synchronized'.format(directory.title())))
                logging.info('Synchronization of {0} complete'.format(dst))
            else:
                logging.info('Synchronization {0} attempted, but {0} is not available'.format(dst, src))
                self.set_color(1.0, 0.0, 0.0)  # Set Display Red
                selected_node.add_node(MenuNode('Please Connect the OP-1   '.format(directory.title())))
        except OSError as e:  # python >2.5
            logging.error('Error during synchronization {0}'.format(e.message))
            self.set_color(1.0, 0.0, 0.0)  # Set Display Red
            selected_node.add_node(MenuNode('Synchronization Failed   '.format(directory.title())))
            pass

    # Touch
    def touch(self, fname):
        now = int(time.time())
        os.utime(fname, (now, now))