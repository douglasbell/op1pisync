#!/usr/bin/env python

"""
Synchronizes the Teenage Engineering OP-1's (https://www.teenageengineering.com/products/op-1) 
album, drum, synh and tape directories (individually or as a whole)
via a Raspberry Pi (https://www.raspberrypi.org/products/raspberry-pi-3-model-b/) with an attached 
AdaFruit 20x4 RGB LCD screen (https://www.adafruit.com/product/498).

When started the LCD will be red indicating the OP-1 has not been detected. WHen connected and placed
into disk mode it will be detected and the screen will turn green. The OP-1 can then be synchronized
by selecting the synchronize menu and selected what is to be synchronized (everything, album, drum,
synth, tape). 

While synchronizing the LCD screen will turn yellow to indicate synchronization is underway. If an 
error occurs the LCD scren will turn red, once complete the screen will return to green and display
a message that synchonization was successfully completed.

Example:
        $ python op1pisync.py -v info
"""

import argparse
import atexit
import logging
import threading
import time
import datetime
import Adafruit_CharLCD as LCD

from watchdog.observers import Observer
from adafruit_lcd_plate_menu import CharMenuDisplay
from OP1Handler import OP1Handler
from Display import Display

# Create and initialize the Display instance
_display = Display()


def manage_display():

    last_button_press_datetime = datetime.datetime.now()
    buttons = [LCD.SELECT, LCD.LEFT, LCD.UP, LCD.RIGHT, LCD.DOWN]
    backlight_off = False

    while True:

        now = datetime.datetime.now()
        last = now - last_button_press_datetime

        if not backlight_off and last.total_seconds() > _display.background_timeout_in_seconds:
            _display.backlight_off()
            backlight_off = True

        for button in buttons:
            if _display.lcd.is_pressed(button):
                last_button_press_datetime = datetime.datetime.now()
                if backlight_off:
                    _display.backlight_on(_display.current_display_color[0],
                                          _display.current_display_color[1],
                                          _display.current_display_color[2])
                    backlight_off = False

        time.sleep(.2)


def op1_watchdog():
    event_handler = OP1Handler()
    observer = Observer()
    observer.schedule(event_handler, path='/dev/disk/by-id', recursive=False)
    observer.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
        pass
    observer.join()


def exit_handler():
    _display.lcd.clear()
    _display.backlight_off()


if __name__ == "__main__":

    # Exit handler
    atexit.register(exit_handler)

    # Parse logging args
    parser = argparse.ArgumentParser(description='Start the OP-1 Synchronization Process')
    parser.add_argument('-v', '--verbosity', required=False, default="critical",
                        choices=['debug', 'info', 'warn', 'error', 'critical'],
                        help='The log level to use.')
    args = parser.parse_args()

    # Set up logging
    numeric_level = getattr(logging, args.verbosity.upper(), None)
    if not isinstance(numeric_level, int):
        print 'Invalid log level: %s. Defaulting to None' % args.log_level
    logging.basicConfig(level=numeric_level,
                        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', datefmt='%Y-%m%-d %I:%M:%S %p')

    # Create and start the thread to manage the display and backlight
    display_thread = threading.Thread(target=manage_display)
    display_thread.daemon = True
    display_thread.start()

    # Start the thread to watch for a connect / disconnect of the OP-1
    watchdog_thread = threading.Thread(target=op1_watchdog)
    watchdog_thread.daemon = True
    watchdog_thread.start()

    # Initialize the menu and display
    menu_nodes = _display.create_menu_nodes()
    CharMenuDisplay(_display.lcd, menu_nodes).display()

