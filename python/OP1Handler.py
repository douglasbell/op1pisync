"""
Watchdog FileSystemEventHandler implementation for the Teenage Engineering OP-1.
Watches for the OP-1 to be connected or disconnected and mounts or dismounts it changing the color of the
AdaFruit LCD from red to green (on connection) or from green to red (on disconnection)
"""

import os
import errno
import logging

from subprocess import *
from watchdog.events import FileSystemEventHandler
from Display import Display


class OP1Handler(FileSystemEventHandler):

    _display = Display()

    mount = '/op-1/mounted/'

    def process(self, event):

        translate = {'created': 'connected',
                     'deleted': 'disconnected',
                     'modified': 'error',
                     'moved': 'error'
                     }

        if 'Teenage_OP-1' in event.src_path:
            logging.info('OP-1 was {0}'.format(translate[event.event_type]))
            if event.event_type == 'created':
                self. _display.set_color(0.0, 1.0, 0.0)  # Set Display Green
            else:
                self. _display.set_color(1.0, 0.0, 0.0)  # Set Display Red

    def on_modified(self, event):
        self.process(event)

    def on_created(self, event):
        self.process(event)
        try:
            if os.path.exists(self.mount):
                os.rmdir(self.mount)
            os.makedirs(self.mount)
            check_call(['mount', event.src_path, self.mount])
            logging.info('OP-1 Mounted to {0}'.format(self.mount))
        except OSError as e:  # Python >2.5
            if e.errno == errno.EEXIST and os.path.isdir(self.mount):
                logging.info('OP-1 Mounted to {0}'.format(self.mount))
            else:
                logging.error('Could not mount the OP-1', e)
            pass

    def on_deleted(self, event):
        self.process(event)
        try:
            if os.path.exists(self.mount):
                check_call(['umount', self.mount])
        except OSError as e:
            logging.error("Could not unmount the OP-1", e)
            pass
        finally:
            if os.path.exists(self.mount):
                os.rmdir(self.mount)

    def on_moved(self, event):
        self.process(event)
